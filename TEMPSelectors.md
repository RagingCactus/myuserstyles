| Var                                   | Selector        | Status |
| ------------------------------------- | --------------- | ------ |
| @selector-chatlist                    | ._1NrpZ         | [x]    |
| @selector-entry                       | ._2EXPL         | [x]    |
| @selector-entry-unread                | .CxUIE          | [x]    |
| @selector-entry-active                | ._1f1zm         | [x]    |
| @selector-chat-title                  | ._25Ooe         | [x]    |
| @selector-chat-timestamp              | ._3Bxar         | [x]    |
| @selector-chat-message-body           | ._itDl          | [x]    |
| @selector-unread-indicator            | .OUeyt          | [x]    |
| @selector-message                     | .vW7d1          | [x]    |
| @selector-message-in                  | .message-in     | [x]    |
| @selector-message-out                 | .message-out    | [x]    |
| @selector-message-system              | .Zq3Mc          | [x]    |
| @selector-message-system-e2e          | ._14b5J         | [x]    |
| @selector-tail-container              | .tail-container | [x]    |
| @selector-message-body                | ._3_7SH         | [x]    |
| @selector-message-quoted-body         | .Y9G3K          | [x]    |
| @selector-text-message-timestamp      | ._2f-RV         | [x]    |
| @selector-media-message-timestamp     | ._1DZAH         | [x]    |
| @selector-voice-message-length        | .DYGf2          | [x]    |
| @selector-sticker-message             | ._1rK-b         | [x]    |
| @selector-sticker-message-bubble      | ._2Ll5c         | [x]    |
| @selector-forwarded-message           | ._15aTv         | [x]    |
| @selector-message-hover-menu-gradient | ._1i1U7         | [x]    |
| @selector-profile-and-settings-panel  | ._3auIg         | [x]    |
| @selector-chatlist-search-panel       | .gQzdc          | [x]    |
| @selector-current-chat-title-panel    | ._2y17h         | [!]    |
| @selector-message-input               | ._3pkkz         | [x]    |
| @selector-welcome-content             | ._3qlW9         | [x]    |

| Status | Symbol                                        |
| ------ | --------------------------------------------- |
| [ ]    | Offen                                         |
| [x]    | OK                                            |
| [-]    | **NICHT** OK                                  |
| [?]    | Nicht nachprüfbar (bzw. schwierig ohne Hilfe) |
| [!]    | Nachgebessert                                 |